<?php
namespace amekusa\WPELib\hook;

class Filter extends Hook {
	
	protected function _register() {
		add_filter($this->hookPoint, $this->callback, $this->priority, $this->nArgs);
	}
	
	protected function _deregister() {
		remove_filter($this->hookPoint, $this->callback, $this->priority, $this->nArgs);
	}
}
?>