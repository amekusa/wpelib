<?php
namespace amekusa\WPELib\hook;

/**
 * A WordPress hook represetation.
 * @author amekusa
 */
abstract class Hook {
	public $callback;
	protected $anchor;
	protected $priority;
	protected $nArgs;
	private $isRegistered = false;
	
	public function __construct($xAnchor, $xCallback, $xPriority = 10, $xNArgs = 1) {
		$this->hookPoint = $xHookPoint;
		$this->callback = $xCallback;
		$this->priority = $xPriority;
		$this->nArgs = $xNArgs;
	}
	
	public function isRegistered() {
		return $this->isRegistered;
	}
	
	public function getHookPoint() {
		return $this->hookPoint();
	}
	
	public final function register() {
		if ($this->isRegistered) throw new Exception('Already Done');
		$this->isRegistered = true;
		$this->_register();
	}
	
	public final function deregister() {
		if (!$this->isRegistered) throw new Exception('Unnecessary Call');
		$this->isRegistered = false;
		$this->_deregister();
	}
	
	public function invoke() {
		$args = func_get_args();
		if (empty($args)) return call_user_func($this->callback);
		return call_user_func_array($this->callback, func_get_args());
	}
	
	protected abstract function _register();
	protected abstract function _deregister();
}
?>