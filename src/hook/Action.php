<?php
namespace amekusa\WPELib\hook;

class Action extends Hook {
	
	protected function _register() {
		add_action($this->hookPoint, $this->callback, $this->priority, $this->nArgs);
	}
	
	protected function _deregister() {
		remove_action($this->hookPoint, $this->callback, $this->priority, $this->nArgs);
	}
}
?>