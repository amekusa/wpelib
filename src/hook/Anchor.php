<?php
namespace amekusa\WPELib\hook;

/**
 * A WordPress hook point representation.
 * @author amekusa <post@amekusa.com>
 * @since 0.0.1.2014.01.06
 */
class Anchor {
	protected $name; // <string>
	protected $nArgs; // <integer>
	protected $hooks; // <array>
	
	/**
	 * @param string $xName
	 * @param integer $xNArgs
	 */
	public function __construct($xName, $xNArgs = 1) {
		$this->name = $xName;
		$this->nArgs = $xNArgs;
		$this->hooks = array ();
	}
	
	public final function accepts(Hook $xHook) {
		if ($xHook->isRegistered()) return false;
		if ($xHook->getHookPoint() != $this->hookPoint) return false;
		return $this->_accepts($xHook);
	}
	
	public function hook($xCallback, $xPriority = 10) {
		$this->_addHook($this->createHook($xCallback, $xPriority));
	}
	
	public final function addHook(Hook $xHook, $xName = null) {
		if (!$this->accepts($xHook)) throw new Exception('Unacceptable Item');
		$this->_addHook($xHook, $xName);
	}
	
	public function removeHook($xName) {
		if (!array_key_exists($xName, $this->hooks))
				throw new Exception('No Such Item');
		
		$this->hooks[$xName]->deregister();
		unset($this->hooks[$xName]);
	}
	
	public function deregisterAll() {
		foreach ($this->hooks as $iH) $iH->deregister();
		$this->hooks = array ();
	}
	
	public abstract function invoke();
	
	protected function _accepts(Hook $xHook) {
		return true;
	}
	
	protected abstract function createHook($xCallback, $xPriority);
	
	protected function _addHook(Hook $xHook, $xName = null) {
		if (empty($xName)) $this->hooks[] = $xHook;
		else {
			if (array_key_exists($xName, $this->hooks))
					throw new Exception('Item Already Exists');
			
			$this->hooks[$xName] = $xHook;
		}
		$xHook->register();
	}
}

class ActionAnchor extends Anchor {
	
	public function invoke() {
		$args = func_get_args();
		if (empty($args)) return do_action($this->hookPoint);
		array_unshift($args, $this->hookPoint);
		return call_user_func_array('do_action', $args);
	}
	
	protected function _accepts(Hook $xHook) {
		return $xHook instanceof Action;
	}
	
	protected function createHook($xCallback, $xPriority) {
		return new Action($this->hookPoint, $xCallback, $xPriority, $this->nArgs);
	}
}

class FilterAnchor extends Anchor {
	
	public function invoke() {
		$args = func_get_args();
		if (empty($args)) return apply_filters($this->hookPoint);
		array_unshift($args, $this->hookPoint);
		return call_user_func_array('apply_filters', $args);
	}
	
	protected function _accepts(Hook $xHook) {
		return $xHook instanceof Filter;
	}
	
	protected function createHook($xCallback, $xPriority) {
		return new Filter($this->hookPoint, $xCallback, $xPriority, $this->nArgs);
	}
}
?>