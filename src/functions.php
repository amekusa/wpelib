<?php
namespace amekusa\WPELib;

use amekusa\PHPKnives as knv;

/**
 * Functions :: WPELib
 * Provides some useful functions for WordPress.
 *
 * @author amekusa <post@amekusa.com>
 */
interface functions {
	const required = true;
}

/**
 * Returns the WP install directory.
 */
function wp_home_dir() {
	static $r = '';
	if (!empty($r)) return $r;
	
	if (function_exists('get_home_path')) $r = get_home_path();
	else if (defined('ABSPATH')) $r = ABSPATH;
	else {
		$x = explode('wp-', getcwd());
		if (!empty($x)) $r = $x[0];
	}
	
	return $r;
}

/**
 *
 * @param unknown $xCallback
 * @param number $xPriority
 * @throws \InvalidArgumentException
 */
function register_theme_setup_func($xCallback, $xPriority = 10) {
	if (!is_callable($xCallback)) throw new \InvalidArgumentException('Uncallable callback: ' . $xCallback);
	add_action('after_setup_theme', $xCallback, $xPriority);
}

/**
 *
 * @param string $xUrl
 * @return string
 */
function inner_url($xUrl = '') {
	$base = preg_replace('/(.*?)\/+$/', '$1', home_url());
	return $base . '/' . preg_replace('/^\/+(.*)/', '$1', $xUrl);
}

function default_query_vars() {
	static $r = array (
			'numberposts' => 5, 
			'offset' => 0, 
			'category' => 0, 
			'orderby' => 'post_date', 
			'order' => 'DESC', 
			'include' => array (), 
			'exclude' => array (), 
			'meta_key' => '', 
			'meta_value' => '', 
			'post_type' => 'post', 
			'suppress_filters' => false);
	
	return $r;
}

function src_of_media($xMediaId, $xSize = 'full') {
	$r = wp_get_attachment_image_src($xMediaId, $xSize);
	return $r[0];
}

function thumbnail_src_of_post($xPost) {
	$r = wp_get_attachment_thumb_url(get_post_thumbnail_id($xPost->ID));
	return $r;
}

/**
 * Returns queried posts array by reference.
 */
function &current_posts_ref() {
	return current_query()->posts;
}

function current_posts() {
	return current_query()->posts;
}

function categories_by_slugs(array $xSlugs) {
	$r = array ();
	foreach ($xSlugs as $n) {
		$nCategory = get_category_by_slug($n);
		if ($nCategory) $r[] = $nCategory;
	}
	return $r;
}

function names_of_post_types(array $xConditions = null) {
	$conds = isset($xConditions) ? $xConditions : array ();
	return get_post_types($conds, 'names');
}

function names_of_custom_post_types(array $xConditions = null) {
	$conds = isset($xConditions) ? $xConditions : array ();
	$conds['_builtin'] = false;
	return names_of_post_types($conds);
}

function url_of_post($xPost) {
	return get_permalink($xPost->ID);
}

function url_of_post_type($xPostType) {
	$type = post_type($xPostType);
	if (!$type) return '';
	return get_post_type_archive_link($type->name);
}

function url_of_taxonomy($xTaxonomy) {
	return get_term_link($xTaxonomy);
}

function url_of_term($xFactor, $xTaxonomyName = '') {
	$x = term($xFactor, $xTaxonomyName);
	if (!isset($x)) return '';
	return get_term_link($x);
}

function url_of_category($xCategory) {
	return get_category_link($xCategory->term_id);
}

function time_of_post($xPost = null) {
	return strtotime(post($xPost)->post_date);
}

function date_of_post($xPost, $xFormat = null) {
	if (empty($xFormat)) $xFormat = get_option('date_format');
	return date($xFormat, time_of_post($xPost));
}

function field_of_post($xPost, $xKey) {
	return get_post_meta($xPost->ID, $xKey, true);
}

function fields_of_post($xPost, $xKey) {
	return get_post_meta($xPost->ID, $xKey, false);
}

function depth_of_post($xPost = null) {
	$ascs = ascendants_of_post($xPost);
	return count($ascs);
}

function add_ajax_action($xActionName, $xFunction) {
	add_action('wp_ajax_' . $xActionName, $xFunction);
	add_action('wp_ajax_nopriv_' . $xActionName, $xFunction);
}

/**
 * Creates and registers a post with specific data.
 *
 * @link http://codex.wordpress.org/Function_Reference/wp_insert_post
 */
function register_post($xData) {

}
?>