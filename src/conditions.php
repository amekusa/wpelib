<?php
namespace amekusa\WPELib;

use amekusa\PHPKnives as knv;

objects::required;

/**
 * Conditions :: WPELib
 */
interface conditions {
	const required = true;
}

function url_is_external($xUrl) {
	if (preg_match('^(\/|\.)', $xUrl)) return false;
	
	$urlHost = parse_url($xUrl, PHP_URL_HOST);
	if (!$urlHost) return false;
	
	$homeHost = parse_url(home_url(), PHP_URL_HOST);
	if (!$homeHost) return false;
	
	return $urlHost != $homeHost;
}

/**
 * Checks whether the plugin is active.
 *
 * @param string $xDirName
 * @param string $xFileName
 * @return boolean
 */
function plugin_is_active($xDirName, $xFileName = '') {
	$activePlugins = apply_filters('active_plugins', get_option('active_plugins'));
	if (empty($activePlugins)) return false;
	
	if (empty($xFileName)) {
		foreach ($activePlugins as $iEach) {
			if (preg_match('/^' . preg_quote($xDirName) . '\//', $iEach)) return true;
		}
		return false;
	}
	
	return in_array($xDirName . '/' . $xFileName, $activePlugins);
}

function post_is_current($xPost) {
	if (!is_singular()) return false;
	return posts_are_equal(get_queried_object(), post($xPost));
}

function post_is_ascendant($xOfPost, $xPost = null) {
	$post = post($xPost);
	$ascs = ascendants_of_post($xOfPost);
	if (!$ascs) return false;
	
	foreach ($ascs as $iAsc) {
		if (posts_are_equal($iAsc, $post)) return true;
	}
	
	return false;
}

function post_is_sibling($xOfPost, $xPost = null) {
	return posts_are_equal(parent_of_post(post($xOfPost)), parent_of_post(post($xPost)));
}

function is_paginated() {
	global $multipage;
	if (empty($multipage)) return false;
	$pagePos = get_query_var('paged');

}

function on_first_page() {
	$x = get_query_var('paged');
	return !$x || $x == 1;
}

function is_ajax() {
	return defined('DOING_AJAX') && DOING_AJAX;
}

function is_post($xObject) {
	if (!is_object($xObject)) return false;
	if (!class_exists('WP_Post')) return $xObject instanceof \stdClass;
	
	return $xObject instanceof \WP_Post;
}

function is_post_type($xObject) {
	if (!is_object($xObject)) return false;
	return true;
}

function is_custom_post_type($xPostType) {
	return !post_type($xPostType)->_builtin;
}

function posts_are_equal($xPost1, $xPost2) {
	return post($xPost1)->ID == post($xPost2)->ID;
}

function post_has_parent($xPost) {
	$post = post($xPost);
	return $post->post_parent != 0;
}
?>