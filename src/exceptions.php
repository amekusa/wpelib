<?php
namespace amekusa\WPELib;

class ObjectNotFoundException extends \RuntimeException {
}

class PostNotFoundException extends ObjectNotFoundException {
	
	public static function create($xFactor) {
		$r = new PostNotFoundException();
		$r->factor = $xFactor;
		return $r;
	}
	
	public $factor;
}
?>