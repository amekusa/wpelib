<?php

// Email validation
add_filter('wpcf7_validate_email', 'wpcf7_validate_email_filter', 11, 2);
add_filter('wpcf7_validate_email*', 'wpcf7_validate_email_filter', 11, 2);

// Form action
add_filter("wpcf7_form_action_url", "wpcf7_form_action_url_filter");

function wpcf7_validate_email_filter($aResult, $aTag) {
	$r = $aResult;
	$type = $aTag['type'];
	$name = $aTag['name'];
	$_POST[$name] = trim(strtr((string) $_POST[$name], "\n", ' '));
	if($type == 'email' || $type == 'email*') {
		if(preg_match('/(.*)-verification$/', $name, $matches)) {
			$x = $matches[1];
			if($_POST[$name] != $_POST[$x]) {
				$r['valid'] = false;
				$r['reason'][$name] = 'メールアドレスをもう一度ご確認ください。';
			}
		}
	}
	return $r;
}

function wpcf7_form_action_url_filter($aRequestUrl) {
	return force_protocol(get_option('home'), 'https').$aRequestUrl;
}

?>
