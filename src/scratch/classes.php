<?php
namespace amekusa\WPELib\scratch;

/**
 * Draft classes
 * @author Satoshi Soma
 */

class HookManager {
	protected $hooks;
	
	public function __construct() {
		$this->hooks = array ();
	}
	
	public function register(Hook $xHook, $xName = null) {
		$this->hooks[] = $xHook;
		$xHook->register();
	}
	
	public function deregister($xName) {
		$this->hooks[$xName]->deregister();
	}
	
	public function deregisterAll() {
		foreach ($this->hooks as $iH) $iH->deregister();
		$this->hooks = array ();
	}
}

/**
 * A WordPress hook
 * @author Soma
 */
abstract class Hook {
	public $callback;
	protected $hookPoint;
	protected $priority;
	protected $nArgs;
	
	public function __construct($xHookPoint, $xCallback, $xPriority = 10, $xNArgs = 1) {
		$this->hookPoint = $xHookPoint;
		$this->callback = $xCallback;
		$this->priority = $xPriority;
		$this->nArgs = $xNArgs;
	}
	
	public abstract function register();
	public abstract function deregister();
}

class Action extends Hook {
	
	public function register() {
		add_action($this->hookPoint, $this->callback, $this->priority, $this->nArgs);
	}
	
	public function deregister() {
		remove_action($this->hookPoint, $this->callback, $this->priority, $this->nArgs);
	}
}

class Filter extends Hook {
	
	public function register() {
		add_filter($this->hookPoint, $this->callback, $this->priority, $this->nArgs);
	}
	
	public function deregister() {
		remove_filter($this->hookPoint, $this->callback, $this->priority, $this->nArgs);
	}
}

class Anchor {
	protected $hooks;
	
	public function __construct() {
		$this->hooks = array ();
	}
	
	public function addHook(Hook $xHook, $xName = null) {
		$this->hooks[] = $xHook;
		$xHook->register();
	}
	
	public function register(Hook $xHook, $xName = null) {
		$this->hooks[] = $xHook;
		$xHook->register();
	}
	
	public function deregister($xName) {
		$this->hooks[$xName]->deregister();
	}
	
	public function deregisterAll() {
		foreach ($this->hooks as $iH) $iH->deregister();
		$this->hooks = array ();
	}
}

class ActionAnchor extends Anchor {
}
?>