<?php
namespace amekusa\WPELib\scratch;

function apply_filters_safely($xHookName, $xTarget) {
	if (!has_filter($xHookName)) return $xTarget;
	return call_user_func_array('apply_filters', func_get_args());
}
?>