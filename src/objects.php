<?php
namespace amekusa\WPELib;

use amekusa\PHPKnives as knv;

conditions::required;

/**
 * Objects
 *
 * @author amekusa <post@amekusa.com>
 */
interface objects {
	const required = true;
}

function current_query() {
	if (isset($GLOBALS['wp_query'])) return $GLOBALS['wp_query'];
	return null;
}

function post($xFactor = null, $xAs = 'object') {
	if (!isset($xFactor)) return current_post();
	if (is_post($xFactor)) return $xFactor;
	
	$r = null;
	
	if (is_numeric($xFactor)) { // Id
		$r = post_by_id($xFactor);
	
	} else if (is_string($xFactor)) { // Slug
		$r = post_by_slug($xFactor);
	}
	
	return $r;
}

function post_by_id($xId) {
	return get_post(knv\int($xId));
}

function post_by_slug($xSlug, $xPostTypes = null) {
	$postTypes = isset($xPostTypes) ? $xPostTypes : names_of_post_types();
	
	$r = get_posts(array (
			'numberposts' => 1, 
			'name' => $xSlug, 
			'post_type' => $postTypes));
	
	return empty($r) ? null : $r[0];
}

function post_by_url($xURL) {
	$r = post_by_id(url_to_postid($xURL));
	if (is_null($r)) throw PostNotFoundException::create($xId);
}

function current_post() {
	if (isset($GLOBALS['post'])) return $GLOBALS['post'];
	else if (isset($GLOBALS['wp_query'])) return $GLOBALS['wp_query']->post;
	return null;
}

function parent_of_post($xPost) {
	return $xPost->post_parent != 0 ? get_post($xPost->post_parent) : null;
}

function root_of_post($xPost) {
	$r = $xPost;
	while ($r->post_parent != 0)
		$r = get_post($r->post_parent);
	return $r;
}

/**
 * #DEPRECATED
 */
function ancestors_of_post($xPost, $xOrder = 'ASC') {
	return ascendants_of_post($xPost, $xOrder);
}

function ascendants_of_post($xPost, $xOrder = 'ASC') {
	$r = array ();
	
	$ancestor = parent_of_post($xPost);
	while ($ancestor) {
		$r[] = $ancestor;
		$ancestor = parent_of_post($ancestor);
	}
	
	if ($xOrder == 'DESC' && !empty($r)) $r = array_reverse($r);
	
	return $r;
}


function children_of_post($xPost, $xOptions = null) {
	return get_posts(children_of_post_qv($xPost, $xOptions));
}

function children_of_post_qv($xPost, $xOptions = null) {
	$post = post($xPost);
	
	$r = array (
			'numberposts' => -1, 
			'post_type' => $post->post_type, 
			'post_parent' => $post->ID);
	
	return empty($xOptions) ? $r : wp_parse_args($xOptions, $r);
}

function siblings_of_post($xPost, $xOptions = null) {
	return get_posts(siblings_of_post_qv($xPost, $xOptions));
}

function siblings_of_post_qv($xPost, $xOptions = null) {
	$post = post($xPost);
	
	$r = array (
			'numberposts' => -1, 
			'post_type' => $post->post_type, 
			'post_parent' => $post->post_parent);
	
	return empty($xOptions) ? $r : wp_parse_args($xOptions, $r);
}

function newest_post($xPostType = 'post') {
	$x = recent_posts($xPostType, 1);
	return empty($x) ? null : $x[0];
}

function oldest_post($xPostType = 'post') {
	$x = older_posts($xPostType, 1);
	return empty($x) ? null : $x[0];
}

/**
 *
 * @param unknown $xCategory
 */
function posts_in_category($xCategory) {
	$category = category($xCategory);
	
	$r = get_posts(array ('numberposts' => -1, 'cat' => $category->term_id));
	
	return $r;
}

function posts_in_term($xTerm, $xOptions = null) {
	return get_posts(posts_in_term_qv($Term, $xOptions));
}

function posts_in_term_qv($xTerm, $xOptions = null) {
	$term = term($xTerm);
	$r = array (
			'numberposts' => -1, 
			'tax_query' => array (
					array (
							'taxonomy' => $term->taxonomy, 
							'filed' => 'term_id', 
							'terms' => $term->term_id)));
	
	if (isset($xOptions)) $r = wp_parse_args($xOptions, $r);
	
	return $r;
}

function recent_posts($xPostType = 'post', $xNPosts = 0) {
	$nPosts = $xNPosts > 0 ? $xNPosts : get_option('posts_per_page');
	return get_posts(array (
			'post_type' => $xPostType, 
			'numberposts' => $nPosts, 
			'posts_per_page' => $nPosts, 
			'orderby' => 'post_date', 
			'order' => 'DESC'));
}

function older_posts($xPostType = 'post', $xNPosts = 0) {
	$nPosts = $xNPosts > 0 ? $xNPosts : get_option('posts_per_page');
	return get_posts(array (
			'post_type' => $xPostType, 
			'numberposts' => $nPosts, 
			'posts_per_page' => $nPosts, 
			'orderby' => 'post_date', 
			'order' => 'ASC'));
}

function thumbnail_of_post($xPost = null) {
	$post = post($xPost);
	if (!has_post_thumbnail($post->ID)) return null;
	
	return post(get_post_thumbnail_id($post->ID));
}

/**
 *
 * @link http://codex.wordpress.org/Function_Reference/get_post_type_object
 *
 * @param mixed $xPostType
 */
function post_type($xPostType) {
	if (is_object($xPostType)) return $xPostType;
	return get_post_type_object($xPostType);
}

/**
 * TODO Work in progress
 */
function current_post_type() {
	
	if (is_single()) {
		return get_post_type_object(current_post()->post_type);
	}
	
	if (is_archive()) {
	
	}
}

function type_of_post($xPost, $xAs = null) {
	$post = post($xPost);
	return get_post_type_object($post->post_type);
}

function post_types(array $xConditions = null) {
	$conds = isset($xConditions) ? $xConditions : array ();
	return get_post_types($conds, 'objects');
}

function category($xFactor) {
	$r = null;
	
	if (is_object($xFactor)) {
		return $xFactor;
	}
	
	if (is_numeric($xFactor)) { // ID
		$r = get_category($xFactor);
	
	} else if (is_string($xFactor)) { // Slug
		$r = get_category_by_slug($xFactor);
	}
	
	return $r;
}

function category_of_post($xPost, $xIndex = 0) {
	$x = get_the_category($xPost->ID);
	return empty($x) ? null : $x[$xIndex];
}

function category_by_slug($xSlug) {
	return get_category_by_slug($xSlug);
}

function current_category() {
	
	if (is_category()) return get_queried_object();
	
	if (is_single() || is_page()) return category_of_post(get_queried_object());
	
	return get_category(get_query_var('cat'));
}

function parent_of_category($xCategory) {
	return get_category($xCategory->parent);
}

function root_of_category($xCategory) {
	$r = $xCategory;
	while ($r->parent != 0)
		$r = get_category($r->parent);
	return $r;
}

function children_of_category($xCategory, $xOptions = null) {
	$x = category($xCategory);
	if (is_null($x)) return array ();
	
	$q = array ('parent' => $x->term_id);
	
	return get_categories(wp_parse_args($xOptions, $q));
}

function term($xFactor, $xTaxonomy = null) {
	$r = null;
	
	if (is_object($xFactor)) { // Term object
		return $xFactor;
	}
	
	$tax = taxonomy($xTaxonomy);
	if (!$tax) return $r;
	
	if (is_numeric($xFactor)) { // ID
		$r = get_term_by('id', $xFactor, $tax->name);
		
	} else if (is_string($xFactor)) { // Slug or Name
		$r = get_term_by('slug', $xFactor, $tax->name);
		if (empty($r)) $r = get_term_by('name', $xFactor, $tax->name);
	}
	
	return $r;
}

function term_of_post($xPost, $xTaxonomy, $xIndex = null) {
	$x = terms_of_post($xPost, taxonomy($xTaxonomy)->name);
	return empty($x) ? null : $x[isset($xIndex) ? $xIndex : key($x)];
}

function term_by_slug($xSlug, $xTaxonomy) {
	return get_term_by('slug', $xSlug, taxonomy($xTaxonomy)->name);
}

function parent_of_term($xTerm = null) {
	$term = term($xTerm);
	if (!$term->parent) return null;
	return get_term($term->parent, $term->taxonomy);
}

function ascendants_of_term($xTerm = null, $xOrder = 'ASC') {
	$r = array ();
	$term = term($xTerm);
	$parent = parent_of_term($term);
	
	while ($parent) {
		if (xOrder == 'ASC') $r[] = $parent;
		else array_unshift($r, $parent);
		$parent = parent_of_term($parent);
	}
	
	return $r;
}

function children_of_term($xTerm = null, $xOptions = null) {
	$term = term($xTerm);
	$defaults = array ('parent' => $term->term_id);
	return terms_in_taxonomy($term->taxonomy, wp_parse_args($xOptions, $defaults));
}

function terms_in_taxonomy($xTaxonomy, $xOptions = null) {
	$taxonomies = array (taxonomy($xTaxonomy)->name);
	$defaults = array ('hide_empty' => false);
	return get_terms($taxonomies, wp_parse_args($xOptions, $defaults));
}

function root_terms_in_taxonomy($xTaxonomy, $xOptions = null) {
	$defaults = array ('parent' => 0);
	return terms_in_taxonomy($xTaxonomy, wp_parse_args($xOptions, $defaults));
}

/**
 * @see http://codex.wordpress.org/Function_Reference/wp_get_object_terms
 */
function terms_of_post($xPost, $xTaxonomy, $xOptions = array ()) {
	$opts = wp_parse_args($xOptions, array ());
	$r = $opts ?
			wp_get_object_terms(post($xPost)->ID, taxonomy($xTaxonomy)->name, $opts) :
			wp_get_object_terms(post($xPost)->ID, taxonomy($xTaxonomy)->name);
	
	return $r;
}

/**
 *
 * @see http://codex.wordpress.org/Function_Reference/get_taxonomy
 */
function taxonomy($xFactor, $xAs = 'object') {
	$r = null;
	
	if (knv\type_matches($xFactor, $xAs)) return $xFactor;
	
	if (is_object($xFactor)) { // Taxonomy object
		return $xFactor;
	}
	
	if (is_string($xFactor)) { // Slug
		$r = get_taxonomy($xFactor);
	}
	
	return $r;
}

/**
 *
 * @param object $xPost
 */
function author_of_post($xPost) {
	return get_userdata(post($xPost)->post_author);
}

function nav_menu_in($xLocation) {
	if (!$xLocation) return false;
	
	$locations = get_nav_menu_locations();
	if (!isset($locations[$xLocation])) return null;
	return get_term($locations[$xLocation], 'nav_menu');
}
?>