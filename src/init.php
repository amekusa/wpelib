<?php
namespace amekusa\WPELib;

// Namespace include guard [
if (defined(__NAMESPACE__.'\LOADED')) return;
const LOADED = true;
// ]

require_once __DIR__.'/exceptions.php';
require_once __DIR__.'/functions.php';
require_once __DIR__.'/objects.php';
require_once __DIR__.'/conditions.php';
require_once __DIR__.'/tags.php';

require_once __DIR__.'/query/package.php';
require_once __DIR__.'/widget/package.php';
require_once __DIR__.'/scratch/package.php';
?>