<?php
namespace amekusa\WPELib\widget;

abstract class Widget extends \WP_Widget {
	
	public static function register() {
		register_widget(get_called_class());
	}
	
	public static function unregister() {
		unregister_widget(get_called_class());
	}
	
	protected $data = array ();
	
	public function __construct() {
		parent::__construct($this->getId(), $this->getName(), $this->getOptions());
	}
	
	/**
	 * Outputs the content of the widget
	 *
	 * @param array $xArgs
	 * @param array $xInstance
	 */
	public function widget($xArgs, $xInstance) {
		echo $xArgs['before_widget'];
		$title = apply_filters('widget_title', $xInstance['title']);
		if (!empty($title)) {
			echo $xArgs['before_title'];
			echo $title;
			echo $xArgs['after_title'];
		}
		echo $this->getBody($xArgs, $xInstance);
		echo $xArgs['after_widget'];
	}
	
	/**
	 * Outputs the options form on admin
	 *
	 * @param array $instance The widget options
	 */
	public function form($instance) {
		if (isset($instance['title'])) {
			$title = $instance['title'];
		} else {
			$title = __('New title');
		}
		
		// @formatter:off [
?>
<p><label for="<?php echo $this->get_field_id('title')?>"><?php _e('Title:')?></label> <input class="widefat" id="<?php echo $this->get_field_id('title')?>" name="<?php echo $this->get_field_name('title')?>" type="text" value="<?php echo esc_attr($title)?>"></p>
<?php
		// ] @formatter:on
	}
	
	/**
	 * Processing widget options on save
	 *
	 * @param array $xNewInstance The new options
	 * @param array $xOldInstance The previous options
	 */
	public function update($xNewInstance, $xOldInstance) {
		return parent::update($xNewInstance, $xOldInstance);
	}
	
	/**
	 *
	 * @return string
	 */
	abstract protected function getId();
	
	/**
	 *
	 * @return string
	 */
	abstract protected function getName();
	
	/**
	 *
	 * @return array
	 */
	protected function getOptions() {
		return array ();
	}
	
	/**
	 * Returns the content of this widget.
	 *
	 * @return string
	 */
	abstract protected function getBody($xArgs, $xInstance);
}
?>