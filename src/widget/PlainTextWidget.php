<?php
namespace amekusa\WPELib\widget;

use amekusa\PHPKnives as knv;

knv\functions::required;

class PlainTextWidget extends Widget {
	
	public function form($xInstance) {
		parent::form($xInstance);
		$text = esc_textarea(knv\enter_array($xInstance, 'text', ''));
		// @formatter:off [
?>
<textarea class="widefat" rows="16" cols="20" id="<?php echo $this->get_field_id('text')?>" name="<?php echo $this->get_field_name('text')?>"><?php echo $text?></textarea>
<?php
		// ] @formatter:on
	}
	
	protected function getId() {
		return 'com_amekusa_wpelib_PlainTextWidget';
	}
	
	protected function getName() {
		return 'Plain Text Widget';
	}
	
	protected function getBody($xArgs, $xInstance) {
		return knv\enter_array($xInstance, 'text', '');
	}
}
?>