<?php
namespace amekusa\WPELib;

use amekusa\PHPKnives as knv;

/**
 * Tags
 *
 * @author amekusa <post@amekusa.com>
 */
interface tags {
	const required = true;
}

function the($xKey, $xDelimiter = ', ') {
	$post = current_post();
	$field = get_post_meta($post->ID, $xKey);
	echo implode($xDelimiter, $field);
}

function the_terms($xTaxonomy) {
	$post = current_post();
	$type = type_of_post($post);
	if (empty($type->taxonomies)) return;
	foreach ($type->taxonomies as $iTax) {
		var_dump($iTax);
	}
}

function the_thumbnail_caption() {
	$r = '';
	$thumbnail = thumbnail_of_post(current_post());
	$r = $thumbnail->post_excerpt;
	echo $r;
}

function the_thumbnail_description() {
	$r = '';
	$thumbnail = thumbnail_of_post(current_post());
	$r = $thumbnail->post_content;
	echo $r;
}

function the_captioned_thumbnail($xSize = 'post-thumbnail', $xAttribs = null, $xContainer = 'figure') {
	$post = current_post();
	
	if (has_post_thumbnail($post->ID)) {
		$thumbnail = post(get_post_thumbnail_id($post->ID));
		
		if (isset($thumbnail)) {
			$caption = $thumbnail->post_excerpt;
			$container = $xContainer;
			if ($container == 'figure') {
			
			}
			$image = get_the_post_thumbnail($post->ID, $xSize, $xAttribs);
			
			$r = <<< EOD
<{$container}>
	{$image}
	<figcaption>{$caption}</figcaption>
</{$container}>
EOD;
			echo $r;
			return;
		}
	}
	
	the_post_thumbnail($xSize, $xAttribs);
}
?>