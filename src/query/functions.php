<?php
namespace amekusa\WPELib\query;

use amekusa\WPELib as Wpe;

/**
 * Functions
 *
 * @author amekusa <post@amekusa.com>
 */
interface functions {
	const required = true;
}

function posts(array $xPosts) {
	$ids = array ();
	foreach ($xPosts as $iPost)
		$ids[] = $iPost->ID;
	$r = array ('post__in' => implode(',', $ids));
	return $r;
}

function rename_field($xFieldName, $xNewName) {
	global $wpdb;
	
	$q = <<< SQL
UPDATE `wp_postmeta`
SET `meta_key` = '{$xNewName}'
WHERE `meta_key` = '{$xFieldName}'
SQL;
	
	$wpdb->query($q);
}
?>