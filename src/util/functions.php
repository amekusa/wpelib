<?php
namespace amekusa\WPELib\util;

function buffer($aCallback) {
	ob_start();
	call_user_func(aCallback);
	return ob_get_clean();
}

?>
