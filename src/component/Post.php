<?php
namespace amekusa\WPELib\component;

use amekusa\WPELib as wpe;
use amekusa\PHPKnives as knv;

wpe\functions::required;
wpe\conditions::required;

class Post extends CompositeComponent implements WebComponent {
	
	public function getTitle() {
		return $this->entity->post_title;
	}
	
	public function getURL() {
		return wpe\url_of_post($this->entity);
	}
	
	protected function getParentEntity() {
		return wpe\parent_of_post($this->entity);
	}
	
	protected function getChildEntities() {
		return wpe\children_of_post($this->entity);
	}
	
	protected function verifyEntity($xEntity) {
		parent::verifyEntity($xEntity);
		if (!wpe\is_post($xEntity)) throw new RuntimeException();
	}
}
?>