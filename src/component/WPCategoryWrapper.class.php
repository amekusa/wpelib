<?php
namespace amekusa\WPELib;

require_once __DIR__.'/functions.php';

/**
 * The WordPress'es category object wrapper
 * @author Satoshi Soma
 * @created 2013-03-14
 * 
 * @modified 2013-03-14 by S.Soma
 */
class WPCategoryWrapper {
	protected $wrappee;
	protected $usesCache;
	
	protected
			$id,
			$name,
			$slug,
			$url,
			$parent,
			$children;
	
	public function __construct($aWrappee, $aUsesCache) {
		$this->usesCache = $aUsesCache;
		$this->wrap($aWrappee);
	}
	
	public function hasParent() {
	}
	
	protected function usesCache() {
		return $this->usesCache;
	}
	
	public function getId() {
		if (!$this->usesCache() || !isset($this->id))
				$this->id = $this->wrappee->cat_ID; // #OR term_ID
		return $this->id;
	}
	
	public function getName() {
		if (!$this->usesCache() || !isset($this->name))
				$this->name = $this->wrappee->name;
		return $this->name;
	}
	
	public function getSlug() {
		if (!$this->usesCache() || !isset($this->slug))
				$this->slug = $this->wrappee->slug;
		return $this->slug;
	}
	
	public function getUrl() {
		if (!$this->usesCache() || !isset($this->url))
				$this->url = get_category_link($this->getId());
		return $this->url;
	}
	
	public function wrap($aWrappee) {
		$this->verifyWrappee($aWrappee);
		if (isset($this->wrappee)) {
			if ($aWrappee == $this->wrappee) return;
			$this->clearCache();
		}
		$this->wrappee = $aWrappee;
	}
	
	protected function verifyWrappee($aWrappee) {
		if (!isset($aWrappee)) echo 'Invalid Wrappee';/*throw new RuntimeException('Invalid Wrappee.')*/;
	}
	
	public function clearCache() {
		unset(
				$this->id,
				$this->url,
				$this->parent,
				$this->children
		);
	}
}
?>
