<?php
namespace amekusa\WPELib;

require_once __DIR__.'/WPCategoryWrapper.class.php';
require_once __DIR__.'/functions.php';

/**
 * The WordPress'es post object wrapper
 * @author Satoshi Soma
 * @created 2013-03-11
 * 
 * @modified 2013-03-14 by S.Soma
 */
class WPPostWrapper {
	protected $wrappee;
	protected $usesCache;
	
	protected
			$id,
			$type,
			$title,
			$content,
			$url,
			$timestamp,
			$categories,
			$parent,
			$children,
			$childrenOrderBy = 'post_date',
			$childrenOrder = 'DESC';
	
	public function __construct($aWrappee, $aUsesCache = true) {
		$this->usesCache = $aUsesCache;
		$this->wrap($aWrappee);
	}
	
	public function isCurrent() {
		return $this->wrappee == current_post();
	}
	
	public function isDescendantOf($aX) {
		$x = $this->getParent();
		while(isset($x)) {
			if($x->wrappee == $aX->wrappee) return true;
			$x = $x->getParent();
		}
		return false;
	}
	
	public function hasParent() {
		return $this->wrappee->post_parent != 0;
	}
	
	protected function usesCache() {
		return $this->usesCache;
	}
	
	public function wraps($aWrappee) {
		return $this->wrappee == $aWrappee;
	}
	
	public function getWrappee() {
		return $this->wrappee;
	}
	
	public function getId() {
		if (!$this->usesCache() || !isset($this->id))
				$this->id = $this->wrappee->ID;
		return $this->id;
	}
	
	public function getType() {
		if (!$this->usesCache() || !isset($this->type))
				$this->type = $this->wrappee->post_type;
		return $this->type;
	}
	
	public function getTitle() {
		if (!$this->usesCache() || !isset($this->title))
				$this->title = $this->wrappee->post_title;
		return $this->title;
	}
	
	public function getContent() {
		if (!$this->usesCache() || !isset($this->content))
				$this->content = $this->wrappee->post_content;
		return $this->content;
	}
	
	public function getUrl() {
		if (!$this->usesCache() || !isset($this->url))
				$this->url = get_permalink($this->wrappee->ID);
		return $this->url;
	}
	
	public function getTimestamp() {
		if (!$this->usesCache() || !isset($this->timestamp))
				$this->timestamp = strtotime($this->wrappee->post_date);
		return $this->timestamp;
	}
	
	public function getCategories() {
		if (!$this->usesCache() || empty($this->categories)) {
			$x = get_the_category($this->getId());
			foreach($x as $n) $this->categories[] = new WPCategoryWrapper($n);
		}
		return $this->categories;
	}
	
	public function getParent() {
		if (!$this->usesCache() || !isset($this->parent)) {
			$x = parent_of_post($this->wrappee);
			if(!isset($x)) return;
			
			$this->parent = new WPPostWrapper($x);
		}
		return $this->parent;
	}
	
	public function getChildren($aOrderBy = null, $aOrder = null) {
		if(!isset($aOrderBy)) $aOrderBy = $this->childrenOrderBy;
		if(!isset($aOrder)) $aOrderBy = $this->childrenOrder;
		
		if(
			!$this->usesCache() ||
			!isset($this->children) ||
			$aOrderBy != $this->childrenOrderBy ||
			$aOrder != $this->childrenOrder
		) {
			$x = children_of_post($this->wrappee, $aOrderBy, $aOrder);
			$this->children = array();
			foreach($x as $n) $this->children[] = new WPPostWrapper($n);
			
			$this->childrenOrderBy = $aOrderBy;
			$this->childrenOrder = $aOrder;
		}
		return $this->children;
	}
	
	public function wrap($aWpPost) {
		$this->verifyWrappee($aWpPost);
		if (isset($this->wrappee)) {
			if ($aWpPost == $this->wrappee) return;
			$this->clearCache();
		}
		$this->wrappee = $aWpPost;
	}
	
	protected function verifyWrappee($aWrappee) {
		if (!isset($aWrappee)) echo 'Invalid Wrappee';/*throw new RuntimeException('Invalid Wrappee.')*/;
	}
	
	public function clearCache() {
		unset(
					$this->id,
					$this->url,
					$this->timestamp,
					$this->parent,
					$this->children
				);
	}
}
?>
