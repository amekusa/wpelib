<?php
namespace amekusa\WPELib\component;

use amekusa\PHPKnives as knv;

/**
 * The base class of WordPress component representations.
 *
 * @author amekusa <post@amekusa.com>
 */
abstract class Component {
	protected $entity;
	
	public function __construct($xEntity, $xIsVerified = false) {
		$this->setEntity($xEntity, $xIsVerified);
	}
	
	public function __get($xName) {
		return knv\get($xName, $this->entity);
	}
	
	public function __call($xName, $xArgs) {
		return call_user_func_array(array ($this->entity, $xName), $xArgs);
	}
	
	public function getEntity() {
		return $this->entity;
	}
	
	protected function setEntity($xEntity, $xIsVerified = false) {
		if (!$xIsVerified) $this->verifyEntity($xEntity);
		$this->entity = $xEntity;
	}
	
	protected function verifyEntity($xEntity) {
		if (is_null($xEntity)) throw new RuntimeException();
	}
}
?>