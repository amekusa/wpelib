<?php
namespace amekusa\WPELib\component;

use amekusa\WPELib as wpe;
use amekusa\PHPKnives as knv;

wpe\functions::required;
wpe\conditions::required;

class PostType extends Component implements WebComponent {
	
	public function getTitle() {
		return $this->entity->label;
	}
	
	public function getURL() {
		return wpe\url_of_post($this->entity);
	}
	
	protected function verifyEntity($xEntity) {
		parent::verifyEntity($xEntity);
		if (!wpe\is_post_type($xEntity)) throw new RuntimeException();
	}
}
?>