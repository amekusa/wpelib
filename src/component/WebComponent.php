<?php
namespace amekusa\WPELib\component;

use amekusa\PHPKnives as knv;

interface WebComponent {
	
	public function getTitle();
	public function getURL();
}
?>