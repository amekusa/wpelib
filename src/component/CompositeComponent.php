<?php
namespace amekusa\WPELib\component;

use amekusa\PHPKnives as knv;

abstract class CompositeComponent extends Component {
	protected $parent;
	protected $children = array ();
	
	public function __construct($xEntity) {
		parent::__construct($xEntity);
		$this->update();
	}
	
	public function hasParent() {
		return isset($this->parent);
	}
	
	public function hasChildren() {
		return !empty($this->children);
	}
	
	public function getParent() {
		return $this->parent;
	}
	
	public function getChildren() {
		return $this->children();
	}
	
	public function update() {
		$x = $this->getParentEntity();
		if (isset($x)) {
			$this->parent = new self($x);
			$parent = new self($x);
		}
		
		$x = $this->getChildEntities();
		foreach ($x as $iX) {
			$this->addChild(new self($iX)); // XXX Need test
		}
	}
	
	protected function setParent($xParent) {
		if ($this->hasParent()) throw new \LogicException('Parent already exists');
		$this->parent = $xParent;
	}
	
	public function addChild($xChild) {
		$this->children[] = $xChild;
		$xChild->setPrent($this);
	}
	
	abstract protected function getParentEntity();
	abstract protected function getChildEntities();
}
?>