<?php
namespace amekusa\WPELib\component;

use amekusa\WPELib as wpe;

wpe\functions::required;

class Query extends Component {
	
	protected function verifyEntity($xEntity) {
		parent::verifyEntity($xEntity);
		if (!$xEntity instanceof \WP_Query) throw new RuntimeException();
	}
}
?>